import { Component } from "react";
import logo from '../data/airplane.png'

/**
 * Class for navigation bar.
 * Handles clicking on airplain and writes down your coordinates.
 */
class Navbar extends Component {
  constructor(props) {
    super(props);
    this.state = { locationText: "getting location" };
  }
  componentDidMount() {
    window.addEventListener('popstate', e => {
      this.forceUpdate();
    });
    window.addEventListener('hashchange', e => {
      this.forceUpdate();
    });
    let img = document.getElementById("airplane");
    img.addEventListener('click', e => {
      window.location.hash = 'main';
    });
	}

  /**
   * Returns image with src based on online state.
   * @returns HTML img
   */
  renderLogo(){
    let img = <img id="airplane" src="https://cdn-icons-png.flaticon.com/512/31/31069.png" alt="airplane"/>;
    if(!window.navigator.onLine) img = <img id="airplane" src={logo} alt="airplane"/>;
    return <div id="airplaneDiv">
        {img}
    </div>
  }

  render() {
    let locationText = "getting location";
    if (this.props.coords !== null){
      locationText = `Coordinations: ${this.props.coords.latitude} ${this.props.coords.longitude}`;
      // locationText = `Latitude : ${this.props.coords.latitude} Longitude: ${this.props.coords.longitude}`;
    }
    return <div className="navbar">
        {this.renderLogo()}
      <p id="location">{locationText}</p>
      <p id="navbarTitle">{this.props.title}</p>
    </div>;
  }
}

export default Navbar;