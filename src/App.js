import React, { Component } from "react";
import { BrowserRouter } from "react-router-dom";
import Navbar from "./components/Navbar";
import MainContainer from "./containers/MainContainer";

/**
 * Main class. Handles document.title changes.
 */
class App extends Component {
  constructor(props) {
    super(props);
    this.state = { title: "USA airport visualization", coords: null};
    this.watchID = null;
  }

  setTitle = (title) => {
    document.title = title;
    this.setState({title: title});
  }

  success(pos) {
    this.setState({coords: pos.coords})
  }
  
  error(err) {
    console.warn(`ERROR(${err.code}): ${err.message}`);
  }

  componentDidMount(){
    if(this.watchID === null)this.watchID = navigator.geolocation.getCurrentPosition(this.success.bind(this), this.error);
  }

  componentWillUnmount(){
    if(this.watchID !== null) navigator.geolocation.clearWatch(this.watchID);
  }

  render() {
    return (
      <BrowserRouter>
        <Navbar title={this.state.title} coords={this.state.coords}/>
        <MainContainer setTitle={this.setTitle} coords={this.state.coords}/>
      </BrowserRouter>
    );
  }
}

export default App;
